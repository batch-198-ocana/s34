const express = require("express");
// mongoose is an ODM library to let our ExpressJS API to manipulate a MongoDB database
const mongoose = require("mongoose");
const app = express();
const port = 4000;

// Mongoose Connection
// mongoose.connect() is a method to connect our api with our mongodb database via the use of mongoose. It has 2 arguments. (1) is the conection string to connect our api to our mongodb. (2) is an object used to add information between mongoose and mongodb
// replace <password> to your db user password
// just before the ? in the connection string, add database name
mongoose.connect("mongodb+srv://adminn:admin123@cluster0.i2dev.mongodb.net/bookingAPI?retryWrites=true&w=majority",{
    useNewUrlParser : true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error',console.error.bind(console, "MongoDB connection error."));
// if the connection is open and successful, we will output a message in the terminal
db.once('open',()=>console.log("Connected to MongoDB"));

app.use(express.json());

// import our routes and use it as middleware which means, that we will be able to group together our routes
const courseRoutes = require('./routes/courseRoutes');
app.use('/courses',courseRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

app.listen(port,()=>console.log(`Server is running at port ${port}`));
