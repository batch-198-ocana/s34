// auth.js is our own module which will contain methods to help authorize or restrict users from accessing certain features in our application

const jwt = require("jsonwebtoken");

// This is the secret string which will validate or which will use to check the validity of a passed token. If a token does not contain this secret string, then that token is invalid or illegitimate.

const secret = "courseBookingAPI";

// JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow access certains of our application.

module.exports.createAccessToken = (userDetails) => {
    console.log(userDetails);

    const data = {
        id:userDetails.id,
        email:userDetails.email,
        isAdmin:userDetails.isAdmin
    }
    console.log(data);
    // jwt.sign() will create a JWT using our data object, with our secret
    return jwt.sign(data,secret,{})
}

module.exports.verify = (req,res,next) => {
    // verify() is going to be used as a middleware, wherein it will be added per route to act as a gate to check if the token being passed is valid or not.
    let token = req.headers.authorization

    if(typeof token === "undefined"){
        return res.send({auth: "Failed. No Token."});
    } else{
        token = token.slice(7)
        jwt.verify(token,secret,function(err,decodedToken){
          if(err){
            return res.send({
                auth:"Failed",
                message:err.message
            })
          }else{
            req.user = decodedToken;
            // next() will let us proceed to the next middleware or controller
            next();
          }
        })
    }
}

module.exports.verifyAdmin = (req,res,next) => {
    if(req.user.isAdmin){
        next();
    } else{
        return res.send({
            auth:"Failed",
            message:"Action Forbidden"
        })
    }
}