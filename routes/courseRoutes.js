// To be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.
// The Router() method, will let us to contain our routes

const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth")
const {verify, verifyAdmin} = auth;

router.get('/',courseControllers.getAllCourses);

router.post('/',verify,verifyAdmin,courseControllers.addCourse);

router.get('/activeCourses',courseControllers.getActiveCourses);

router.get('/getSingleCourse/:id',courseControllers.getSingleCourse)

router.put('/updateCourse/:courseId',verify,verifyAdmin,courseControllers.updateCourse);

router.delete('/archiveCourse/:courseId',verify,verifyAdmin,courseControllers.archiveCourse);

module.exports = router;