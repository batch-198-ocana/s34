const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");
const {verify} = auth;

router.post("/",userControllers.registerUser);

router.post("/details",verify,userControllers.getUserDetails);

// route for user Authentication
router.post('/login',userControllers.loginUser);

router.post('/checkEmail',userControllers.checkEmail);

// user enrollment
router.post('/enroll',verify,userControllers.enroll);

module.exports = router;