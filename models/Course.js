const mongoose = require("mongoose");

// mongoose schema : before we can create documents from our api to save into our database, we must first determine the structure of the documents to be written in the database

// schema acts as a blueprint for our data/document

// a schema is  a representation of how the document is structured. It also determines the types of data and the expected properties.

// So, with this, we wont have to worry for if we had input "stock" or "stocks" in our documents, because we can make it uniform with a schema

// in mongoose, create a schema, we use the Schema() constructor from mongoose. This will allow us to create a new mongoose schema object.

const courseSchema = new mongoose.Schema({

    name:{
        type:String,
        required:[true,"Name is required"]
    },
    description:{
        type:String,
        required:[true,"Description is required"]
    },
    price:{
        type:Number,
        required:[true,"Price is required"]
    },
    isActive:{
        type:Boolean,
        default:true
    },
    createdOn:{
        type:Date,
        default:new Date()
    },
    enrollees:[
        {
            userID:{
                type:String,
                required:[true,"User ID is required"]
            },
            dateEnrolled:{
                type:Date,
                default:new Date()
            },
            status:{
                type:String,
                default:"Enrolled"
            }
        }
    ]
})

// module.exports : so we can import and use this file in another file
// mongoose.model is the connection to our collection. It has 2 args, (1) the name of the collection (2) is the schema of the documents in the collection.
module.exports = mongoose.model("Course",courseSchema);