const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (req,res)=>{

    const hashedPw = bcrypt.hashSync(req.body.password,10);
    let newUser = new User({
        firstName:req.body.firstName,
        lastName:req.body.lastName,
        email:req.body.email,
        password:hashedPw,
        mobileNo:req.body.mobileNo
    })

    newUser.save()
    .then(result=>res.send(result))
    .catch(error=>res.send(error))
}

module.exports.getUserDetails = (req,res)=>{
    
    User.findById(req.user.id)
    .then(result=>res.send(result))
    .catch(error=>res.send(error))
}

module.exports.loginUser = (req,res) =>{

    User.findOne({email:req.body.email})
    .then(foundUser=>{
        if(foundUser === null){
            return res.send({message: "No User Found."});
        } else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
            if(isPasswordCorrect){
                return res.send({accessToken:auth.createAccessToken(foundUser)});
            } else {
                return res.send({message: "Username and/or password is incorrect."});
            }
        }
    })
    .catch(error=>res.send(error))
}

module.exports.checkEmail = (req,res) =>{
    User.findOne({email:req.body.email})
    .then(result=>{
        if(result === null){
            return res.send(false);
        } else {
            return res.send(true);
        }
    })
    .catch(error=>res.send(error))
}

module.exports.enroll = async (req,res) =>{

    if(req.user.isAdmin){
        return res.send({message:"Action Forbidden."});
    } else {
        // async keyword is added to a function to make our function asynchronous.
        // await keyword allows us to wait for the function to finish and get a result before proceeding.
        let isUserUpdated = await User.findById(req.user.id).then(user=>{
            let newEnrollment = {
                courseID:req.body.courseId
            }
            user.enrollments.push(newEnrollment);
            return user.save().then(user=>true).catch(err=>err.message)
        })
        if(isUserUpdated !== true){
            return res.send({message: isUserUpdated});
        } 

        let isCourseUpdated = await Course.findById(req.body.courseId).then(course=> {
            let enrollee = {
                userID:req.user.id
            }
            course.enrollees.push(enrollee);
            return course.save().then(course=>true).catch(err=>err.message)
        })
        if(isCourseUpdated !== true){
            return res.send({message: isCourseUpdated});
        }

        if(isUserUpdated && isCourseUpdated){
            return res.send({message:"Thank you for enrolling!"});
        }
    }
}